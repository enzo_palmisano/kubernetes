[main]
name = "PrivateBin AGEPoly test instance"
discussion = true
opendiscussion = false
password = true
fileupload = false
burnafterreadingselected = false
defaultformatter = "plaintext"
sizelimit = 10485760
template = "bootstrap"
languageselection = false

[expire]
default = "1week"

[expire_options]
5min = 300
10min = 600
1hour = 3600
1day = 86400
1week = 604800
1month = 2592000
1year = 31536000
never = 0

[formatter_options]
plaintext = "Plain Text"
syntaxhighlighting = "Source Code"
markdown = "Markdown"

[traffic]
limit = 10
dir = PATH "data"

[purge]
limit = 300
batchsize = 10
dir = PATH "data"

[model]
class = Database

[model_options]
dsn = "mysql:host=databases;dbname=privatebin_test;charset=UTF8"
tbl = "privatebin_test_"	; table prefix
usr = "privatebin_test"
pwd = "privatebin_test"
opt[12] = true	  ; PDO::ATTR_PERSISTENT
