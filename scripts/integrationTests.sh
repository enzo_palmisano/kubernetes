#!/bin/sh
set -e

cat <<-EOF
I would be returning once I have tested that all the services deployed in
this environment are available and functionnal. In particular I would have
challenged the services to access their databases and persistent volumes.

Unfortunately, these tests are not yet implemented and I cannot get any
feedback, so I'm assumng everything went well.
EOF
