{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is part of Silhouette

module Silhouette.Readers.SecretSpec where

import Control.Applicative
import Data.Aeson
import Data.Aeson.TH (deriveJSON, deriveToJSON, defaultOptions)
import Data.Aeson.Types (Parser)
import Data.Foldable
import Data.HashMap.Strict (member, HashMap)
import Data.Text (Text)

data DetachedSecret =
    SecretEnvVar { name :: Text, value :: Text }
  | SecretFile { path :: FilePath, file :: FilePath }
    deriving (Show, Eq)

instance FromJSON DetachedSecret where
  parseJSON = withObject "Detached Secret" secret
    where secret v | member "value" v = SecretEnvVar <$> v .: "name" <*> v .: "value"
          secret v | member "path" v = SecretFile <$> v .: "path" <*> v .: "file"
          secret v = fail "Could not recognise secret type."

$(deriveToJSON defaultOptions ''DetachedSecret)

data ApplicationSecrets = ApplicationSecrets
  { appName :: Text
  , secrets :: HashMap Text [DetachedSecret]
  } deriving (Show, Eq)

newtype SecretsFile = SecretsFile [ApplicationSecrets] deriving (Show, Eq)

$(deriveJSON defaultOptions ''ApplicationSecrets)
$(deriveJSON defaultOptions ''SecretsFile)
