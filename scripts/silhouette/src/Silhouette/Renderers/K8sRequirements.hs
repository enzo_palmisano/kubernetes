{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Werror=missing-fields #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is part of Silhouette

module Silhouette.Renderers.K8sRequirements where

import Data.List (union)
import Data.Maybe
import Data.Text (Text)
import Silhouette.Types (AppConfiguration(..), AppSecrets(..))

import qualified Data.Text as T
import qualified Silhouette.Readers.ApplicationSpec as AS
import qualified Silhouette.TemplateManagement as TM

data Reqs = Reqs
  { rPullSecrets :: [Text]
  , rSecrets :: [Text]
  , rVolumes :: [Text]
  , rTlsSecrets :: [Text]
  } deriving (Show, Eq)

instance Semigroup Reqs where
  a <> b = Reqs (m rPullSecrets) (m rSecrets) (m rVolumes) (m rTlsSecrets)
    where m field = field a `union` field b

instance Monoid Reqs where
  mempty = Reqs [] [] [] []

computeReqs :: AppConfiguration -> AppSecrets -> Reqs
computeReqs AppConfiguration{..} AppSecrets{..} = Reqs
  { rPullSecrets = let getSecrets TM.Deployment{..} = imagePullSecrets
                   in getSecrets deployment
  , rSecrets = let getName TM.Secret{..} = secretName
                   hasSecretAtoms TM.Secret{..} = length secrets > 0
               in map getName . filter hasSecretAtoms $ secrets
  , rVolumes = let getName TM.PersistentVolume{..} = name
               in map getName volumes
  , rTlsSecrets = let getName TM.TlsSecret{..} = Just name
                      getName _ = Nothing
                  in mapMaybe (getName . TM.tlsSource) ingressRoute
  }

renderSection :: Text -> [Text] -> Text
renderSection sectionName elems = T.unlines $ ("@" <> sectionName) : elems

unzipWith :: (a -> b -> c) -> [(a, b)] -> [c]
unzipWith = map . uncurry

renderReqs :: Reqs -> Text
renderReqs Reqs{..} = T.unlines $ unzipWith renderSection
  [ ("image-pull-secrets", rPullSecrets)
  , ("secrets", rSecrets)
  , ("persistent-volumes", rVolumes)
  , ("tls-secrets", rTlsSecrets)
  ]
