#!/usr/bin/env sh
#
# (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
#     Téo Goddet, Roosembert Palacios, 2019
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# shellcheck disable=2164

# Exit codes
E_USER=1

die()
{
    retval="$1"; shift
    if [ $# -eq 0 ]; then
        cat <&0 >&2
    else
        printf "%s" "$@" >&2; echo >&2
    fi
    if [ "$retval" = $E_USER ]; then
        echo "Run with --help for more information." >&2
    fi
    (cleanup || true) >/dev/null 2>&1
    exit "$retval"
}

BASENAME="$0"

usage()
{
  cat <<-EOF
	$BASENAME: Verify the specified requirement is available in the cluster.

	Usage:
	$BASENAME [-h]

	$BASENAME -n namespace -f ./reqs.txt -t type --kubeconfig kubeconfigpath

	Description:
	  This program will verify that the specified requirements are available in
	  the cluster.

	Supported requirement types:
	  - image-pull-secrets
	  - secrets
	  - persistent-volumes
	  - tls-secrets

	Options
	  -h, --help
	      Show this help message and exit.
	  -n, --namespace
	      Namespace where the config is applied.
	  -f, --requirements-file
	      Path to the requirements file.
	  -t, --requirement-type
	      Verify the specified requirement type from the requirements file.
	  --kubeconfig
	      kubeconfig where to deploy

	Example:
	  $BASENAME -n staging -f ./requirements.txt -t tls-secrets
	      This command will verify all the tls-secrets specified in ./requirements.txt
	    are available in namespace staging.

	EOF
}


while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
        (-h|--help) usage; exit ;;
        (-n|--namespace) NAMESPACE="$1"; shift ;;
        (-f|--requirements-file) REQ_FILE="$1"; shift ;;
        (-t|--requirement-type) REQ_TYPE="$1"; shift ;;
        (--kubeconfig) KUBECONFIG_PATH="$1"; shift ;;
        (--create-namespace) CREATE_NAMESPACE=1; shift ;;
        (-*) die $E_USER 'Unknown option: %s' "$opt" ;;
        (*) die $E_USER 'Trailing argument: %s' "$opt" ;;
    esac
done

if ! [ -f "$REQ_FILE" ]; then
  die $E_USER "Please specify a requirements file."
fi

if [ -z "$REQ_TYPE" ]; then
  die $E_USER "Please specify the requirement type to verify."
fi

FIFO="$(mktemp)"
rm "$FIFO"
mkfifo "$FIFO"

cleanup()
{
  rm "$FIFO"
}

section_image_pull_secrets()
{
  RET=0
  echo "Verifying image pull secrets"
  while read -r NAME; do
    echo "- $NAME"
    if ! kubectl -n "$NAMESPACE" get secret --field-selector type=kubernetes.io/dockerconfigjson | grep "$NAME" > /dev/null; then
      RET=1
      echo "Requirement failed for $NAME" >&2
    fi
  done < "$FIFO"
  return $RET
}

section_secrets()
{
  RET=0
  echo "Verifying secrets"
  while read -r NAME; do
    echo "- $NAME"
    if ! kubectl -n "$NAMESPACE" get secret "$NAME" > /dev/null; then
      RET=1
      echo "Requirement failed for $NAME" >&2
    fi
  done < "$FIFO"
  return $RET
}

section_persistent_volumes()
{
  RET=0
  echo "Verifying persistent volumes"
  while read -r NAME; do
    echo "- $NAME"

    if STATUS="$(kubectl -n "$NAMESPACE" get pv "$NAME" | awk 'NR==2 && //{print $5}')"; then
      continue # Non-existent persistent volumes are acceptable
    fi

    case "$STATUS" in
      (BOUND) continue ;;
      (AVAILABLE) continue ;;
      (*)
        echo "Invalid status for persistent volume $NAME: $STATUS"
        RET=1
        echo "Requirement failed for $NAME" >&2
        ;;
    esac
  done < "$FIFO"
  return $RET
}

section_tls_secrets()
{
  RET=0
  echo "Verifying tls secrets"
  while read -r NAME; do
    echo "- $NAME"
    if ! kubectl -n "$NAMESPACE" get secret --field-selector type=kubernetes.io/tls | grep "$NAME" > /dev/null; then
      RET=1
      echo "Requirement failed for $NAME" >&2
    fi
  done < "$FIFO"
  return $RET
}

if [ -n "$KUBECONFIG_PATH" ]; then
  echo "Using kube config located at: $KUBECONFIG_PATH"
  export KUBECONFIG="$KUBECONFIG_PATH"
fi

awk "/^@${REQ_TYPE}/{a=1;next} /^$/{a=0} a && //{print}" <"$REQ_FILE" >"$FIFO" &

case "$REQ_TYPE" in
  (image-pull-secrets) section_image_pull_secrets ;;
  (secrets) section_secrets ;;
  (persistent-volumes) section_persistent_volumes ;;
  (tls-secrets) section_tls_secrets ;;
  (*) die $E_USER 'Unknown requirement type: %s' "$opt" ;;
esac

RET=$?

if [ $RET -eq 0 ]; then
  echo "Requirement verification successful."
fi

cleanup
exit $RET
